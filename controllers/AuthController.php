<?php

namespace Controllers;

use Libs\File;
use Libs\Helper;
use Models\User;

class AuthController extends Controller
{

    public function login()
    {
        return $this->response->view('auth/login');
    }

    public function login_()
    {
        $err = [];

        $email = $this->request->post('email');
        $validate = $this->validateEmail($email);
        if ($validate !== true) {
            $err [] = $validate;
        }

        $password = $this->request->post('password');
        $validate = $this->validatePassword($password);
        if ($validate !== true) {
            $err [] = $validate;
        }

        if (empty($err)) {
            $userModel = new User();
            $password = Helper::makeHash($password);
            $user = $userModel->getUserByCredentials($email, $password);
            if ($user === null) {
                $err[] = 'err_credentials';
            }
        }

        if (!empty($err)) {
            return $this->response->view('auth/login', compact('err', 'email'));
        }

        $this->request->session->clearCsrf();
        $this->request->session->setUserId($user['id']);
        $this->request->session->setLand($user['lang']);
        $this->response->redirect('/');
    }

    public function register()
    {
        return $this->response->view('auth/register');
    }

    public function logout_()
    {
        /* lang специально не очищаем */
        $this->request->session->setUserId();
        $this->request->session->clearCsrf();
        $this->response->redirect('/login');
    }

    public function register_()
    {
        $err = [];

        $email = $this->request->post('email');
        $validate = $this->validateEmail($email);
        if ($validate !== true) {
            $err [] = $validate;
        }

        $name = $this->request->post('name');
        $validate = $this->validateName($name);
        if ($validate !== true) {
            $err [] = $validate;
        }

        $about = $this->request->post('about');
        $validate = $this->validateAbout($about);
        if ($validate !== true) {
            $err [] = $validate;
        }

        $password = $this->request->post('password');
        $validate = $this->validatePassword($password);
        if ($validate !== true) {
            $err [] = $validate;
        }

        if (empty($err)) {
            $password_conf = $this->request->post('password_conf');
            if ( $password != $password_conf ) {
                $err [] = 'password_mismatch';
            }
        }

        if (empty($err)) {
            $userModel = new User();
            $user = $userModel->getUserByEmail($email);
            if ($user !== null) {
                $err[] = 'email_busy';
            }
        }

        if (empty($err)) {
            $userModel = new User();
            $password = Helper::makeHash($password);
            $userId = $userModel->createNewUser($name, $about, $email, $password, $this->request->session->getLand());
            $this->request->session->setUserId($userId);
            if ( $this->request->file() !== null ) {
                $file_data = $this->request->file();
                $file_hash = File::uploadAndReturnHash($file_data['tmp_name']);
                //$ext = pathinfo($file_data['name'], PATHINFO_EXTENSION);
                $userModel->updateFile($userId, $file_data['name'], $file_hash, $file_data['type']);
            }
            $this->response->redirect('/');
        }

        if (!empty($err)) {
            return $this->response->view('auth/register', compact('err', 'email', 'name'));
        }
        
        Helper::dd($this->request->file());
    }


}
