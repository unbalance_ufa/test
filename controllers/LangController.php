<?php

namespace Controllers;

use Libs\Helper;
use Models\User;

class LangController extends Controller
{

    public function toRu()
    {
        $this->setLang('ru');
        $this->response->redirect('/');
    }

    public function toEn()
    {
        $this->setLang('en');
        $this->response->redirect('/');
    }

    private function setLang($lang)
    {
        $this->request->session->setLand($lang);

        /* проверяем авторизацию */
        $userId = $this->request->session->getUserId();
        if ( empty($userId) ) {
            return;
        }
        $userModel = new User();
        $user = $userModel->getById($userId);
        if ( empty($user) ) {
            return;
        }
        /* записываем в Бд язык */
        $userModel->setLang($userId, $lang);
    }


}
