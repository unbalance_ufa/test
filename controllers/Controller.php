<?php

namespace Controllers;

use Libs\Request;
use Libs\Response;

class Controller
{

    public $request = null;
    public $response = null;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->response = new Response();
    }

    public function validateEmail($email)
    {
        if (empty($email)) {
            return 'email_required';
        }
        if (strlen($email) > 255) {
            return 'email_long';
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return 'email_bad_format';
        }
        return true;
    }

    public function validatePassword($password)
    {
        if (empty($password)) {
            return 'password_required';
        }
        if (strlen($password) > 255) {
            return 'password_long';
        }
        if (strlen($password) < 5) {
            return 'password_small';
        }
        /* тут можно добавить проверки на сложность */
        return true;
    }

    public function validateName($name)
    {
        if (empty($name)) {
            return 'name_required';
        }
        if (strlen($name) > 255) {
            return 'name_long';
        }
        if (strlen($name) < 2) {
            return 'name_small';
        }
        return true;
    }

    public function validateAbout($name)
    {
        if (strlen($name) > 1200) {
            return 'about_long';
        }
        return true;
    }

}