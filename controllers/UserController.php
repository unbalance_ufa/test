<?php

namespace Controllers;

use Libs\Helper;
use Libs\Request;
use Models\User;

class UserController extends Controller
{

    private $user;
    private $userModel;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $userId = $this->request->session->getUserId();
        if ( empty($userId) ) {
            $this->response->redirect('/login');
        }
        $this->userModel = new User();
        $this->user = $this->userModel->getById($userId);
        if ( empty($this->user) ) {
            $this->response->redirect('/login');
        }
    }

    public function index()
    {
        return $this->viewProfile($this->user);
    }

    public function viewProfile($user)
    {
        $name = $user['name'];
        $email = $user['email'];
        $file = $user['file_hash'];
        $about = $user['about'];
        return $this->response->view('user/profile', compact('name', 'email', 'file', 'about'));
    }

    public function profile()
    {
        $user = $this->userModel->getById($this->request->get('id'));
        if ( empty($user) ) {
            Helper::dd(404);
        }
        return $this->viewProfile($user);
    }

    public function file()
    {
        $file_hash = $this->request->get('f');
        if ( empty($file_hash) ) {
            Helper::dd('404');
        }
        $file = $this->userModel->getFileData($file_hash);
        return $this->response->file($file['file_origin'], $file_hash, $file['file_type']);
    }

    public function userList()
    {
        $users = $this->userModel->getAll();
        return $this->response->view('user/list', compact('users'));
    }

}
