<?php

/*
 * Без динамических url
 * [http_method][url][namespace\class@method]
 *
 * */

return
    [
        'GET' => [
            '/' => 'Controllers\UserController@index',
            '/login' => 'Controllers\AuthController@login',
            '/register' => 'Controllers\AuthController@register',
            '/file' => 'Controllers\UserController@file',
            '/list' => 'Controllers\UserController@userList',
            '/profile' => 'Controllers\UserController@profile',
        ],
        'POST' => [
            '/lang/ru' => 'Controllers\LangController@toRu',
            '/lang/en' => 'Controllers\LangController@toEn',
            '/login' => 'Controllers\AuthController@login_',
            '/logout' => 'Controllers\AuthController@logout_',
            '/register' => 'Controllers\AuthController@register_',
        ]
    ];