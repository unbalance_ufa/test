<?php

namespace Libs;

class Helper {

    static function dd(...$params)
    {
        foreach ($params as $param) {
            echo '<pre>' . json_encode($param) . '</pre>';
        }
        die();
    }

    static function error($code, ...$params)
    {
        http_response_code($code);
        foreach ($params as $param) {
            echo '<pre>' . json_encode($param) . '</pre>';
        }
        die();
    }

    static function env($key)
    {
        $env = include('../env.php');
        return $env[$key] ?? null;
    }

    static function makeHash($value)
    {
        /* можно заменить на любую функцию */
        return md5($value . Helper::env('password_key'));
    }

}
