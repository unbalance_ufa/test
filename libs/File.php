<?php

namespace Libs;

use Models\User;

class File {

    static function uploadAndReturnHash($tmp_name)
    {
        $userModel = new User();
        do {
            $tmp_hash = md5(time() . $tmp_name . rand(111111, 999999));
            $issetHash = $userModel->issetHashFile($tmp_hash);
        } while($issetHash);

        if (move_uploaded_file($tmp_name, __DIR__ . '/../storage/' . $tmp_hash)) {
            return $tmp_hash;
        } else {
            Helper::error(500,'Error upload file');
        }


    }


}