<?php

namespace Libs;

use PDO;

class Database {

    public $db;

    public function __construct () {
        $this->db  = new PDO(
            'mysql:host=' . Helper::env('DB_HOST') . ';dbname=' . Helper::env('DB_NAME'),
            Helper::env('DB_USER'),
            Helper::env('DB_PASS'),
            [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => true,
            ]
        );
    }


}