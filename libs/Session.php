<?php

namespace Libs;

/*
 * так как это php сессии, то можно создавать объект в любом месте и данные будут актуальны
 * */

class Session {

    public function __construct()
    {
        $this->start();
    }

    public function getCsrf()
    {
        if ( empty($_SESSION['csrf']) ) {
            // можно заменить на любую функию хэширования или шифрования
            $_SESSION['csrf'] = md5(Helper::env('csrf_key') . time());
        }
        return $_SESSION['csrf'];
    }

    private function start()
    {
        session_start();
    }

    public function clearCsrf()
    {
        $_SESSION['csrf'] = null;
    }

    public function setUserId($id = null)
    {
        $_SESSION['userId'] = $id;
    }

    public function getUserId()
    {
        return $_SESSION['userId'] ?? null;
    }

    public function getLand($default = 'ru')
    {
        return $_SESSION['land'] ?? $default;
    }

    public function setLand($lang)
    {
        $_SESSION['land'] = $lang;
    }

}