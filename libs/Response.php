<?php

namespace Libs;

/*
 * Класс для стандартизирования ответов контроллера
 * $data должен вызывться с помощью compact
 * */

class Response
{

    public $view = '';
    public $type = 'view';
    public $file_name = '';
    public $file_hash = '';
    public $file_type = '';
    public $code = 200;
    public $data = [];

    public function view($view, $data = [])
    {
        $this->type = 'view';
        $this->view = $view;
        $this->data = $data;
        return $this;
    }

    public function file($file_name, $file_hash, $file_type)
    {
        $this->type = 'file';
        $this->file_name = $file_name;
        $this->file_hash = $file_hash;
        $this->file_type = $file_type;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getView()
    {
        return $this->view;
    }

    public function redirect($url)
    {
        header('Location: ' . $url);
        die();
    }

}