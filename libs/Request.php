<?php

namespace Libs;

class Request {

    private $get;
    private $post;
    private $file;
    public $session;

    public function __construct()
    {
        $this->get = $this->filterData($_GET);
        $this->post = $this->filterData($_POST);
        /* ожидаемый файл с именем file */
        $this->file = $this->filterFile($_FILES['file']??null);
        $this->session = new Session();
    }

    /* т.к. по ТЗ будет всего 1 файл, то только его и проверяем */
    private function filterFile($file)
    {
        if ( empty($file) ) {
            return null;
        }
        if ( $file['error'] != 0 ) {
            return null;
        }
        if ($file['size'] > Helper::env('max_file_size')) {
            return null;
        }
        if ( !in_array($file['type'], ['image/png', 'image/jpeg', 'image/gif']) ) {
            return null;
        }
        return $file;
    }

    private function filterData($arr = [])
    {
        $filtered = [];
        foreach ($arr as $k => $v) {
            $filtered[ $this->filterVal($k) ] = $this->filterVal($v);
        }
        return $filtered;
    }

    private function filterVal($str)
    {
        $str = trim($str);
        $str = stripslashes($str);
        $str = htmlspecialchars($str);
        return $str;
    }

    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function getURL()
    {
        $s = $_SERVER['REQUEST_URI'];
        if ( strpos($s, '?') ) {
            return substr($s, 0, strpos($s, '?'));
        }
        return $s;
    }

    public function file()
    {
        return $this->file ?? null;
    }

    public function get($key)
    {
        return $this->get[$key] ?? null;
    }

    public function post($key)
    {
        return $this->post[$key] ?? null;
    }

}
