<?php

namespace Libs;

/*
Предоставляет интерфейсы для всех вьюшек.
Внутри вью будет доступен объект по $this
Каждая вью может загрузить другую (например, для цикличного вывода) и передать в нее данные.
Данный, полученные из контроллера передаются во все вью, а так же у вью есть доступ к response
Каждая вью может переопределить шаблон
 */

class View {

    private $response;
    private $session;
    private $layout = 'default';
    private $title = '';
    private $lang = 'ru';
    private $lang_data = [];

    public function __construct( Response $response )
    {
        $this->response = $response;
        $this->session = new Session();
        $this->lang = $this->session->getLand();
        if ( !file_exists('../frontend/lang/' . $this->lang . '.php') ) {
            $this->lang = 'ru';
        }
        $this->lang_data = include ('../frontend/lang/' . $this->lang . '.php');
    }

    private function setLayout($name)
    {
        $this->layout = $name;
    }

    public function getHtml()
    {
        /* вызов изначальной вью, которую запросил контроллер */
        ob_start();
        $this->renderView($this->response->getView(), $this->response->getData());
        $html_views = ob_get_contents();
        ob_end_clean();

        /* вызов шаблона и рендер $html_view */
        ob_start();
        include('../frontend/layouts/' . $this->layout . '.php');
        $html_result = ob_get_contents();
        ob_end_clean();
        return $html_result;
    }

    private function renderView($view, $data = [])
    {
        extract($this->response->getData());
        extract($data);
        include('../frontend/views/' . $view . '.php');
    }

    /* функция для файлов языков */
    private function __($text)
    {
        return $this->lang_data[$text] ?? $text;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

}
