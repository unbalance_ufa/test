<?php

namespace Libs;

class App {

    private $request;
    private $response;
    private $session;

    public function __construct()
    {
        $this->session = new Session();
        $this->request = new Request();
        $this->execRequest($this->request->getMethod(), $this->request->getURL());
    }

    private function execRequest($method, $url)
    {
        $routes = include ('../routes.php');
        if ( empty($routes[$method][$url]) ) {
            Helper::error(404, 'Page no found');
        }
        $className = explode('@', $routes[$method][$url])[0];
        $methodName = explode('@', $routes[$method][$url])[1];
        if ( !class_exists($className) ) {
            Helper::error(500,'No controller');
        }
        $controller = new $className($this->request);
        if ( !method_exists($controller, $methodName) ) {
            Helper::error(500,'No method in controller');
        }
        /* если запрос post, то проверяем csrf */
        if ( $method == 'POST' ) {
            if ( $this->request->post('csrf') !== $this->session->getCsrf() ) {
                Helper::error(401,'Error csrf');
            }
        }
        $this->response = $controller->{$methodName}();
    }

    public function sendResponse()
    {
        if ( $this->response->type == 'view' ) {
            $view = new View($this->response);
            $html = $view->getHtml();
            echo $html;
            die();
        }
        if ( $this->response->type == 'file' ) {
            header('Content-Description: File Transfer');
            header('Content-Type: ' . $this->response->file_type);
            header('Content-Disposition: attachment; filename=' . $this->response->file_name);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize(__DIR__ . '/../storage/' . $this->response->file_hash));
            readfile(__DIR__ . '/../storage/' . $this->response->file_hash);
            exit;
        }
    }

}
