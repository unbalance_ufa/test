$(document).ready(function () {

    $('.check_lang_action').on('click', function () {
        val = $(this).data('val');
        $('#check_lang_form').attr('action', '/lang/' + val).submit();
        return false;
    });

    $('#logout_action').on('click', function () {
        $('#logout_form').submit();
    })

});