<?php

namespace Models;

use Libs\Database;
use Libs\Helper;

/*
 * Выполнено в виде методов, которые решают бизнес-задачи
 * */

class User extends Database
{

    public function getAll()
    {
        $res = $this->db->prepare('SELECT * FROM `users`');
        $res->execute([]);
        $users = $res->fetchAll();
        return $users;
    }

    public function getUserByCredentials($email, $password)
    {
        $res = $this->db->prepare('SELECT * FROM `users` WHERE `email` = ? and `password` = ? LIMIT 0,1');
        $res->execute([
            $email,
            $password,
        ]);
        if ( $res->rowCount() === 0 ) {
            return null;
        }
        $user = $res->fetch();
        return $user;
    }

    public function getById($id)
    {
        $res = $this->db->prepare('SELECT * FROM `users` WHERE `id` = ? LIMIT 0,1');
        $res->execute([
            $id,
        ]);
        if ( $res->rowCount() === 0 ) {
            return null;
        }
        $user = $res->fetch();
        return $user;
    }

    public function getUserByEmail($email)
    {
        $res = $this->db->prepare('SELECT * FROM `users` WHERE `email` = ? LIMIT 0,1');
        $res->execute([
            $email,
        ]);
        if ( $res->rowCount() === 0 ) {
            return null;
        }
        $user = $res->fetch();
        return $user;
    }

    public function setLang($id, $lang)
    {
        $res = $this->db->prepare('UPDATE `users` SET `lang` = ?, `updated_at` = ? WHERE `id` = ?');
        $res->execute([
            $lang,
            time(),
            $id,
        ]);
    }

    public function updateFile($id, $file_origin, $file_hash, $file_type)
    {
        $res = $this->db->prepare('UPDATE `users` SET `file_origin` = ?, `file_hash` = ?, `file_type` = ?, `updated_at` = ? WHERE `id` = ?');
        $res->execute([
            $file_origin,
            $file_hash,
            $file_type,
            time(),
            $id,
        ]);
    }

    public function createNewUser($name, $about, $email, $password, $lang)
    {
        $res = $this->db->prepare('INSERT INTO `users` SET `name` = ?, `about` = ?, `email` = ?, `password` = ?, `lang` =?, `created_at`=?, `updated_at` = ? ');
        $res->execute([
            $name,
            $about,
            $email,
            $password,
            $lang,
            time(),
            time(),
        ]);
        return $this->db->lastInsertId();
    }

    public function issetHashFile($hash)
    {
        $res = $this->db->prepare('SELECT * FROM `users` WHERE `file_hash` = ? LIMIT 0,1');
        $res->execute([
            $hash,
        ]);
        if ( $res->rowCount() === 0 ) {
            return false;
        }
        return true;
    }

    public function getFileData($hash)
    {
        $res = $this->db->prepare('SELECT `file_hash`, `file_origin`, `file_type` FROM `users` WHERE `file_hash` = ? LIMIT 0,1');
        $res->execute([
            $hash,
        ]);
        if ( $res->rowCount() === 0 ) {
            return false;
        }
        return $res->fetch();
    }

}
