<?php
/**@var $this Libs\View */
/**@var $html_views String */
?>
<!DOCTYPE html>
<html lang="<?= $this->lang ?>">
<head>
    <title><?= $this->getTitle() ?></title>
    <? $this->renderView('assets'); ?>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <? $this->renderView('check_lang', ['csrf' => $this->session->getCsrf()]); ?>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/login"><?=$this->__('login')?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/register"><?=$this->__('register')?></a>
            </li>
        </ul>
    </div>

</nav>

<?= $this->renderView('errors', $err ?? []); ?>

<?= $html_views; ?>

</body>
</html>
