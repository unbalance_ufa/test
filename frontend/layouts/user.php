<?php
/**@var $this Libs\View */
/**@var $html_views String */
?>
<!DOCTYPE html>
<html lang="<?= $this->lang ?>">
<head>
    <title><?= $this->getTitle() ?></title>
    <? $this->renderView('assets'); ?>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <? $this->renderView('check_lang', ['csrf' => $this->session->getCsrf()]); ?>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/list"><?=$this->__('users_list')?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/"><?=$this->__('profile_my')?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="logout_action" href="#"><?=$this->__('logout')?></a>
            </li>
            <form id="logout_form" method="post" action="/logout">
                <input type="hidden" name="csrf" value="<?=$this->session->getCsrf()?>">
            </form>
        </ul>
    </div>

</nav>

<?= $html_views; ?>


</body>
</html>
