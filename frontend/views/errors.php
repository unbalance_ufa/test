<?php if (!empty($err)) { ?>
    <div class="alert alert-danger" role="alert">
        <? foreach ($err as $item) { ?>
            <p><?=$this->__($item);?></p>
        <?php } ?>
    </div>
<?php } ?>