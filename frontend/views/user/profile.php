<?php
/**@var $this \Libs\View */
/**@var $file String */
/**@var $name String */
/**@var $email String */
/**@var $about String */
$this->setLayout('user');
?>

<div class="container mt-5">
    <div class="row">
        <div class="col">
            <h3 class="card-title"><?=$this->__('profile')?></h3>
        </div>
    </div>
</div>


<div class="container mt-5">
    <div class="row">
        <div class="col">
            <table class="table">
                <tr>
                    <th><?=$this->__('name')?></th>
                    <td><?=$name?></td>
                </tr>
                <tr>
                    <th><?=$this->__('email')?></th>
                    <td><?=$email?></td>
                </tr>
                <? if (!empty($file)) { ?>
                    <tr>
                        <th><?=$this->__('profile_image')?></th>
                        <td><img class="profile_image" src="/file?f=<?= $file ?>" class="card-img-top" alt="file"></td>
                    </tr>
                <? } ?>
                <tr>
                    <th><?=$this->__('about')?></th>
                    <td><?=$about?></td>
                </tr>
            </table>
        </div>
    </div>
</div>


