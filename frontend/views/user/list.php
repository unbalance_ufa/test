<?php
/**@var $this \Libs\View */
/**@var $users ArrayObject */
$this->setLayout('user');
?>

<div class="container mt-5">
    <div class="row">
        <div class="col">
            <h3 class="card-title"><?= $this->__('users_list') ?></h3>
        </div>
    </div>
</div>


<div class="container mt-5">
    <div class="row">
        <div class="col">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th><?=$this->__('name_in_list')?></th>
                    <th><?=$this->__('email')?></th>
                    <th></th>
                </tr>
                <?
                $i = 0;
                foreach ($users as $user) {
                $i++;
                ?>
                    <tr>
                        <th><?= $i ?></th>
                        <td><?= $user['name'] ?></td>
                        <td><?= $user['email'] ?></td>
                        <td><a href="/profile?id=<?=$user['id']?>"><?=$this->__('open_profile')?></a></td>
                    </tr>
                <? } ?>

            </table>
        </div>
    </div>
</div>


