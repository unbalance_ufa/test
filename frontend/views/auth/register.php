<?php
/**@var $this \Libs\View*/
$this->setTitle($this->__('register'));
?>

<div class="container mt-5">
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <?=$this->__('register')?>
                </div>
                <div class="card-body">
                    <form enctype="multipart/form-data" method="post" action="/register">
                        <?$this->renderView('csrf_input');?>
                        <div class="form-group">
                            <label for="name"><?=$this->__('name')?>*</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?=$name??''?>" required>
                        </div>
                        <div class="form-group">
                            <label for="email"><?=$this->__('email')?>*</label>
                            <input type="email" class="form-control" id="email" name="email" value="<?=$email??''?>" required>
                        </div>
                        <div class="form-group">
                            <label for="password"><?=$this->__('password')?>*</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                        <div class="form-group">
                            <label for="password_conf"><?=$this->__('password_conf')?>*</label>
                            <input type="password" class="form-control" id="password_conf" name="password_conf" required>
                        </div>
                        <div class="form-group">
                            <label for="file"><?=$this->__('file')?></label>
                            <input type="hidden" name="MAX_FILE_SIZE" value="<?=\Libs\Helper::env('max_file_size')?>" />
                            <input type="file" class="form-control-file" id="file" name="file" accept="image/png, image/jpeg, image/gif">
                            <small><?=$this->__('fill_once')?></small>
                        </div>
                        <div class="form-group">
                            <label for="about"><?=$this->__('about')?></label>
                            <textarea class="form-control" id="about" rows="3" name="about"></textarea>
                            <small><?=$this->__('fill_once')?></small>
                        </div>
                        <button type="submit" class="btn btn-primary"><?=$this->__('sign_up')?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
