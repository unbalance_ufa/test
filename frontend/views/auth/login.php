<?php
/**@var $this \Libs\View*/
$this->setLayout('default');
$this->setTitle($this->__('login'));
?>

<div class="container mt-5">
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <?=$this->__('login')?>
                </div>
                <div class="card-body">
                    <form method="post">
                        <input type="hidden" name="csrf" value="<?=$this->session->getCsrf()?>">
                        <div class="form-group">
                            <label for="email"><?=$this->__('email')?></label>
                            <input type="email" class="form-control" id="email" name="email" value="<?=$email??''?>" required>
                        </div>
                        <div class="form-group">
                            <label for="password"><?=$this->__('password')?></label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                        <button type="submit" class="btn btn-primary"><?=$this->__('sign_in')?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
